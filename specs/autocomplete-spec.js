"use strict"

require("../src/js/autocomplete.js")

describe('makeReq', function() {
  beforeEach(function() {
    var autoComplete = new autoComplete();
  })
  describe('makeReq', function() {
    beforeEach(function() {
      spyOn(autoComplete, 'makeReq');
    })
    
    it('should make a call to populate suggestions if requested fruit exists', function() {
      expect(autoComplete.makeReq('cher')).toHaveBeenCalledWith('cherry', 'cher');
    });

    it('should not call populate suggestions if req fruit doesn\'t exist', function() {
      expect(autoComplete.makeReq('thi')).not.toHaveBeenCalled()
    });

  });

  describe('highlightMatch', function() {
    it('should wrap the query in strong tags if match is made', function() {
      var highlightText = autoComplete.highlightMatch('cherry', 'cher')
      expect(highlightText).toEqual('<strong>cher<strong>ry');
    })
  });

})
