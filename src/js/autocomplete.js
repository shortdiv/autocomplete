
var AutoComplete = (function() {
var data = ['Apple', 'Orange', 'Banana', 'Pineapple', 'Blueberry', 'Blackberry', 'Raspberry', 'Cranberry', 'Clementine', 'Mango', 'Papaya', 'Peach', 'Tangerine', 'Pear', 'Plum', 'Grapes', 'Boysenberry', 'Lychee', 'Pomegranate', 'Watermelon', 'Honey Dew Melon', 'Fig', 'Cherry', 'Grapefruit'],
			MOUSE = {
				RETURN: 13,
				UP: 38,
				RIGHT: 39,
				DOWN: 40,
				ESCAPE: 27
			},
			selectedIndex = -1,
			suggestion = 'autocomplete-suggestion',
			$suggestionsContainer = $('.autocomplete-box'),
			$searchField = $('.searchField'),
			$autoCompleteBox = $('.autocomplete-box'),
			activeItem,
			searchTerm;

	$searchField
		.keyup(function(e) {
			if (e.which === MOUSE.DOWN) {
				e.preventDefault();
				moveDown();
			} else if (e.which === MOUSE.UP) {
				e.preventDefault();
				moveUp();
			} else if(e.which === MOUSE.RIGHT) {
				clearPrevSearch();
			} else if(e.which === MOUSE.RETURN) {
				clearPrevSearch();
				autoComplete(activeItem);
			} else if(e.which === MOUSE.ESCAPE) {
				clearPrevSearch();
			} else {
				clearPrevSearch();
				var query = e.target.value;      
				makeReq(query);
			}
	});

	$autoCompleteBox
		.mouseover(function(e) {
			selectedIndex = $(e.target).index();
			suggestions = $suggestionsContainer.children();
			
			activeItem = suggestions[selectedIndex];
			$('.active').removeClass('active');
			$(e.target).addClass('active');
			autoComplete(activeItem);
		})
		.mouseleave(function(e) {
			clearPrevSearch();
		})
		.click(function(e){
			activeItem = suggestions[selectedIndex];
			$('.active').removeClass('active');
			$(e.target).addClass('active');
			autoComplete(activeItem);
			clearPrevSearch();
	});

	//////// HELPER FUNCTIONS ////////


	function makeReq(query) {
		var requestedFruit = [];
		query = query.toLowerCase().replace(/\s+/g, "");
		for(var i = 0; i < data.length; i++ ){
			fruit = data[i].toLowerCase();
			if(_.include(fruit, query)) {
				requestedFruit.push(data[i]);
			} 
		}
		if(requestedFruit.length > 0) {
			$('.autocomplete-box').addClass('with-suggestions'); 
			populateSuggestions(requestedFruit, query);
		}
	}

	function moveUp() {
		var active = 'active';
		if(selectedIndex > 0) {
			$('.active').removeClass(active);
			selectedIndex--;
			activeItem = suggestions[selectedIndex];
			$(activeItem).addClass(active);
			autoComplete(activeItem);
		} else if(selectedIndex === 0) {
			$searchField.val(searchTerm);
		} else {
			return null;
		}
	}

	function moveDown() {
		var active = 'active';
		suggestions = $suggestionsContainer.children();
		
		if((suggestions.length -1) > selectedIndex) {
			$('.active').removeClass('active');
			selectedIndex++;
			activeItem = suggestions[selectedIndex];
			
			$(activeItem).addClass(active);
			autoComplete(activeItem);
		} else {
			return null;
		}
	}

	function clearPrevSearch() {
		selectedIndex = -1;
		$('.autocomplete-box').empty()
			.removeClass('with-suggestions');
	}

	function populateSuggestions(requestedFruit, query) {
		var dataIndex = 0;
		
		requestedFruit.map(function(fruit) {
			var matches = highlightMatch(fruit, query),
					html = '<div class="autocomplete-suggestion" data-index="' + dataIndex + '"></div>';
				
		$('.autocomplete-box').append(
			$(html).html(matches)
		);
		});
	}


	function highlightMatch(fruit, query) {
		return fruit.replace(new RegExp("(" + query + ")",'gi'), "<strong>$1<\/strong>");
	}

	function autoComplete(selectedItem) {
		//grab original item
		var selectedFruit = $(selectedItem).html(),
				showFormattedFruit = selectedFruit.replace(/\<(\/?strong)\>/g, "");
		
		//replace
		$searchField.val(showFormattedFruit);
	}

  return {
    makeReq: makeReq
  }
});

var hello = function() {
  console.log('hello');
}

module.exports = AutoComplete;
