module.exports = function(gulp, cfg, env) {
  var jasmine = require('gulp-jasmine');
  
  gulp.task('tests', function() {
    gulp.src('specs/*.js')
      .pipe(jasmine());
  })
}
