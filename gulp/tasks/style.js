module.exports = function(gulp, cfg, env) {
  var sass = require('gulp-sass');
  
  gulp.task('style', function() {
    gulp.src(cfg.paths.styleIn + '*.scss')
      .pipe(sass().on('error', sass.logError))
      .pipe(gulp.dest(cfg.paths.styleOut));
  });

  gulp.task('style:watch', function () {
    gulp.watch(cfg.paths.styleIn + '*.scss', ['style']);
  });
}
