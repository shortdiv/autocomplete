module.exports = function(gulp, cfg, env) {
  gulp.task('copy', function() {
    return gulp.src(cfg.paths.scriptsIn + '*.js')
      .pipe(gulp.dest(cfg.paths.scriptsOut))
  })
} 
