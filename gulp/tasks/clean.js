module.exports = function(gulp, cfg, env) {
  var del = require('del');

  gulp.task('clean', function() {
    return del([cfg.paths.dest + '**/*'])
  })
}
