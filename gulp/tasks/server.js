module.exports = function(gulp, cfg, env) {
  var connect = require('gulp-connect');

  gulp.task('server', function() {
    connect.server({
      root: '.',
      port: 8000
    })
  })
}
