module.exports = function(gulp, cfg, env) {
  var browserify = require('browserify'),
      source = require('vinyl-source-stream');

	gulp.task('browserify', function () {
		return browserify('./src/js/autocomplete.js')
			.bundle()
			.pipe(source('autocomplete.js'))
			.pipe(gulp.dest('./build/js'));
	});
}
