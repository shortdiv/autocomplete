var gulp = require('gulp'),
    _ = require('lodash'),
    runSequence = require('run-sequence').use(gulp),
    requireDir = require('require-dir'),
    config = require('./config/paths');

//Load Tasks
tasks = requireDir("./gulp/tasks/")

// Get environment, for environment-specific activities
env = process.env.NODE_ENV || "development"

//Create tasks
_.each(tasks, function(task) {
  task(gulp, config, env);
})

gulp.task('build-setup', function(cb) {
  runSequence('clean', 'browserify', cb)
})

gulp.task('build',['build-setup'], function(cb) {
	runSequence('style', 'tests', cb)
});

gulp.task('default', function(cb) {
  runSequence('build', 'server', cb)
})
