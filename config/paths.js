var tinylr = require('tiny-lr')();

module.exports = {
  livereload: tinylr,
  files: {},
  paths: {
    config: 'config/',
    dest: 'build/',
    tests: 'specs/',
    scriptsIn: 'src/js/',
    scriptsOut: 'build/js/',
    styleIn: 'src/sass/',
    styleOut: 'build/css/'
  }
}
